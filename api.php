<div class="panel panel-inverse">
    <div class="panel-heading">
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
        </div>
        <h4 class="panel-title">Data Table - Default</h4>
    </div>
    <div class="panel-body">
<table id="data-table" class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>No</th>
            <th>Name</th>
            <th>Url</th>
        </tr>
    </thead>
    <tbody>


<?php

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "http://165.22.82.105:8080/listCategories?page=0&size=5",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_POSTFIELDS => array('name' => 'Versace for real i\'m cobra','url' => 'https://vladmihalcea.com/wp-content/uploads/2018/01/HPJP_Video_Vertical.jpg'),
));

$response = curl_exec($curl);

curl_close($curl);

  $decoded = json_decode($response, true);
  $n = 0;
	foreach($decoded as $value)
	{
    $n++;
      echo '<tr>';
      echo '<td>'.$n.'</td>';
      echo '<td>'.$value['name'].'</td>';
      echo '<td>'.$value['url'].'</td>';
      echo '</tr>';
  }
?>

    </tbody>
</table>
</div>
</div>
