 <?php
    if (isset($_GET['id'])) 
    {
        $apiUrl = 'http://165.22.82.105';
        $postId = $_GET['id'];    
        $url        = $apiUrl.'/community/'.$postId;
        // CALL MTN
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_PORT => "8080",
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 360,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
            "Content-Type: application/x-www-form-urlencoded"
            ),
        ));

        $response = json_decode(curl_exec($curl));
        $err = curl_error($curl);
        $contentList = $response->{'contentList'};

        curl_close($curl);
         for ($i = 0; $i < 1; $i++){$pageImage = $contentList[$i]->{'content'}; }
    }  
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <meta property="og:url"                content="http://afrishop.rw/community/<?php echo $postId;?>" />
    <meta property="og:type"               content="article" />
    <meta property="og:title"              content="<?php echo $response->{'title'};?>" />
    <meta property="og:description"        content="<?php echo $response->{'caption'};?>" />
    <meta property="og:image"              content="<?php echo $pageImage;?>" />
   
    <title> Afrishop</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500" rel="stylesheet">
    <!-- Core CSS -->
    <link rel="stylesheet" href="assets/css/bulma.css">
    <link rel="stylesheet" href="assets/css/app.css">
    <link rel="stylesheet" href="assets/css/core.css">
<script
  src="https://code.jquery.com/jquery-3.5.1.min.js"
  integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
  crossorigin="anonymous"></script>
    <style>
* {box-sizing: border-box}
body {font-family: Verdana, sans-serif; margin:0}
.mySlides {display: none}
img {vertical-align: middle;}

/* Slideshow container */
.slideshow-container {
  max-width: 1000px;
  position: relative;
  margin: auto;
}

/* Next & previous buttons */
.prev, .next {
  cursor: pointer;
  position: absolute;
  top: 50%;
  width: auto;
  padding: 16px;
  margin-top: -22px;
  color: white;
  font-weight: bold;
  font-size: 18px;
  transition: 0.6s ease;
  border-radius: 0 3px 3px 0;
  user-select: none;
}

/* Position the "next button" to the right */
.next {
  right: 0;
  border-radius: 3px 0 0 3px;
}

/* On hover, add a black background color with a little bit see-through */
.prev:hover, .next:hover {
  background-color: rgba(0,0,0,0.8);
}

/* Caption text */
.text {
  color: #f2f2f2;
  font-size: 15px;
  padding: 8px 12px;
  position: absolute;
  bottom: 8px;
  width: 100%;
  text-align: center;
}

/* Number text (1/3 etc) */
.numbertext {
  color: #f2f2f2;
  font-size: 12px;
  padding: 8px 12px;
  position: absolute;
  top: 0;
}

/* The dots/bullets/indicators */
.dot {
  cursor: pointer;
  height: 15px;
  width: 15px;
  margin: 0 2px;
  background-color: #bbb;
  border-radius: 50%;
  display: inline-block;
  transition: background-color 0.6s ease;
}

.active, .dot:hover {
  background-color: #717171;
}

/* Fading animation */
.fade {
  -webkit-animation-name: fade;
  -webkit-animation-duration: 1.5s;
  animation-name: fade;
  animation-duration: 1.5s;
}

@-webkit-keyframes fade {
  from {opacity: .4} 
  to {opacity: 1}
}

@keyframes fade {
  from {opacity: .4} 
  to {opacity: 1}
}

/* On smaller screens, decrease text size */
@media only screen and (max-width: 300px) {
  .prev, .next,.text {font-size: 11px}
}

img:before {
    content: ' ';
    display: block;
    position: absolute;
    height: 50px;
    width: 50px;
    background-image: url(assets/img/placeholder.png);
</style>
</head>

<body class="is-white">
    <div id="main-navbar" class="navbar is-inline-flex is-transparent no-shadow is-hidden-mobile" style="background-color: #fce406;">
        <div class="container is-fluid">
            <div class="navbar-brand">
                <a href="#" class="navbar-item">
                    <img src="assets/img/logo/afrishop.png" height="50" alt="">
                </a>
            </div>
            <div class="navbar-menu">
                <div class="navbar-start"> </div>
    
                <div class="navbar-end">
    
                    <div class="navbar-item">
                        <div id="global-search" class="control">
                            <input id="tipue_drop_input" class="input is-rounded" type="text" placeholder="Search" required>
                            <span id="clear-search" class="reset-search">
                                <i data-feather="x"></i>
                            </span>
                            <span class="search-icon">
                                <i data-feather="search"></i>
                            </span>
                        </div>
                    </div>
                    <div class="navbar-item is-cart">
                        <div class="cart-button">
                            <i data-feather="shopping-cart"></i>
                            <div class="cart-count">
                                <span>3</span>
                            </div>
                        </div>
                    
                        <!-- Cart dropdown -->
                        <div class="shopping-cart">
                            <div class="cart-inner">
                    
                                <!--Loader-->
                                <div class="navbar-cart-loader is-active">
                                    <div class="loader is-loading"></div>
                                </div>
                    
                                <div class="shopping-cart-header">
                                    <a href="#" class="cart-link">View Cart</a>
                                    <div class="shopping-cart-total">
                                        <span class="lighter-text">Total:</span>
                                        <span class="main-color-text">$193.00</span>
                                    </div>
                                </div>
                                <!--end shopping-cart-header -->
                        
                                <ul class="shopping-cart-items">
                                    <li class="cart-row">
                                        <img src="assets/img/products/2.svg" alt="" />
                                        <span class="item-meta">
                                            <span class="item-name">Cool Shirt</span>
                                            <span class="meta-info">
                                                <span class="item-price">$29.00</span>
                                                <span class="item-quantity">Qty: 01</span>
                                            </span>
                                        </span>
                                    </li>
                        
                                    <li class="cart-row">
                                        <img src="assets/img/products/3.svg" alt="" />
                                        <span class="item-meta">
                                            <span class="item-name">Military Short</span>
                                            <span class="meta-info">
                                                <span class="item-price">$39.00</span>
                                                <span class="item-quantity">Qty: 01</span>
                                            </span>
                                        </span>
                                    </li>
                        
                                    <li class="cart-row">
                                        <img src="assets/img/products/4.svg" alt="" />
                                        <span class="item-meta">
                                            <span class="item-name">Cool Backpack</span>
                                            <span class="meta-info">
                                                <span class="item-price">$125.00</span>
                                                <span class="item-quantity">Qty: 01</span>
                                            </span>
                                        </span>
                                    </li>
                                </ul>
                        
                                <a href="#" class="button primary-button is-raised">Checkout</a>
                            </div>
                        </div>
                    </div>
                    <div id="account-dropdown" class="navbar-item is-account drop-trigger has-caret">
                        <div class="user-image">
                            <img  src="https://via.placeholder.com/400x400" data-demo-src="assets/img/avatars/jenna.png" alt="">
                            <span class="indicator"></span>
                        </div>
                    
                        <div class="nav-drop is-account-dropdown">
                            <div class="inner">
                                <div class="nav-drop-header">
                                    <span class="username">Your Name</span>
                                    <a href="#">Profile</a>
                                </div>
                                <div class="nav-drop-body account-items">
                                    <a href="#" class="account-item">
                                        <div class="media">
                                            <div class="icon-wrap">
                                                <i data-feather="settings"></i>
                                            </div>
                                            <div class="media-content">
                                                <h3>Settings</h3>
                                                <small>Access account settings.</small>
                                            </div>
                                        </div>
                                    </a>
                                    <a class="account-item">
                                        <div class="media">
                                            <div class="icon-wrap">
                                                <i data-feather="life-buoy"></i>
                                            </div>
                                            <div class="media-content">
                                                <h3>Help</h3>
                                                <small>Contact our support.</small>
                                            </div>
                                        </div>
                                    </a>
                                    <a class="account-item">
                                        <div class="media">
                                            <div class="icon-wrap">
                                                <i data-feather="power"></i>
                                            </div>
                                            <div class="media-content">
                                                <h3>Log out</h3>
                                                <small>Log out from your account.</small>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    
    <nav class="navbar mobile-navbar is-hidden-desktop" aria-label="main navigation">
        <!-- Brand -->
        <div class="navbar-brand">
            <a class="navbar-item" href="#">
                <img src="assets/img/logo/afrishop.png" alt="">
            </a>
    
            <!-- Mobile menu toggler icon -->
            <div class="navbar-burger">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
        <!-- Navbar mobile menu -->
        <div class="navbar-menu">
            <!-- Account -->
            <div class="navbar-item has-dropdown is-active">
                <div class="navbar-link">
                    <img src="https://via.placeholder.com/150x150" data-demo-src="assets/img/avatars/jenna.png" alt="">
                    <span class="is-heading">Name</span>
                </div>
    
                <!-- Mobile Dropdown -->
                <div class="navbar-dropdown">
                    <a href="#" class="navbar-item is-flex is-mobile-icon">
                        <span><i data-feather="user"></i>Profile</span>
                    </a>
                    <a href="#" class="navbar-item is-flex is-mobile-icon">
                        <span><i data-feather="shopping-cart"></i>Cart</span>
                        <span class="menu-badge">3</span>
                    </a>
                    <a href="#" class="navbar-item is-flex is-mobile-icon">
                        <span><i data-feather="settings"></i>Settings</span>
                    </a>
                    <a href="#" class="navbar-item is-flex is-mobile-icon">
                        <span><i data-feather="hexagon"></i>Logout</span>
                    </a>
                </div>
            </div>
        </div>
    </nav>
    <div class="view-wrapper is-full">
        <div class="stories-wrapper is-home">
            <!-- /html/partials/pages/stories/stories-sidebar.html -->
            <div class="stories-sidebar is-active">
                <div class="stories-sidebar-inner">
                    <div class="user-block">
                        <a class="close-stories-sidebar is-hidden">
                            <i data-feather="x"></i>
                        </a>
                        <div class="avatar-wrap">
                            <img src="https://via.placeholder.com/150x150" data-demo-src="assets/img/avatars/jenna.png" data-user-popover="0"
                                alt="">
                            <div class="badge">
                                <i data-feather="check"></i>
                            </div>
                        </div>
                        <h4>Full Name</h4>
                        <p>Member Type</p>
                        <div class="user-stats">
                            <div class="stat-block">
                                <span>Followers</span>
                                <span>2.3K</span>
                            </div>
                            <div class="stat-block">
                                <span>Following</span>
                                <span>1.3K</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <!-- Stories -->
            <div class="inner-wrapper" id="pageContent">
                 

                Loadding...
            </div>
            <div id="product-quickview" class="modal product-quickview is-large has-light-bg">
                <div class="modal-background quickview-background"></div>
                <div class="modal-content">
            
                    <div class="card">
                        <div class="quickview-loader is-active">
                            <div class="loader is-loading"></div>
                        </div>
                        <div class="left">
                            <div class="product-image is-active">
                                <img src="assets/img/products/1.svg" alt="">
                            </div>
                        </div>
                        <div class="right">
                            <div class="header">
                                <div class="product-info">
                                    <h3 id="quickview-name">Product Name</h3 id="quickview-price">
                                    <p>Product tagline text</p>
                                </div>
                                <div id="quickview-price" class="price">
                                    27.00
                                </div>
                            </div>
                            <div class="properties">
                                <!--Colors-->
                                <div id="color-properties" class="property-group is-hidden">
                                    <h4>Colors</h4>
                                    <div class="property-box is-colors">
                                        <!--item-->
                                        <div class="property-item">
                                            <input type="radio" name="quickview_colors" id="red">
                                            <div class="item-inner">
                                                <div class="color-dot">
                                                    <div class="dot-inner is-red"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--item-->
                                        <div class="property-item">
                                            <input type="radio" name="quickview_colors" id="blue">
                                            <div class="item-inner">
                                                <div class="color-dot">
                                                    <div class="dot-inner is-blue"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--item-->
                                        <div class="property-item">
                                            <input type="radio" name="quickview_colors" id="green">
                                            <div class="item-inner">
                                                <div class="color-dot">
                                                    <div class="dot-inner is-green"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--item-->
                                        <div class="property-item">
                                            <input type="radio" name="quickview_colors" id="yellow">
                                            <div class="item-inner">
                                                <div class="color-dot">
                                                    <div class="dot-inner is-yellow"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
            
                                <!--Colors-->
                                <div id="size-properties" class="property-group">
                                    <h4>Sizes</h4>
                                    <div class="property-box is-sizes">
                                        <!--item-->
                                        <div class="property-item">
                                            <input type="radio" name="quickview_sizes" id="S">
                                            <div class="item-inner">
                                                <span class="size-label">S</span>
                                            </div>
                                        </div>
                                        <!--item-->
                                        <div class="property-item">
                                            <input type="radio" name="quickview_sizes" id="M" checked>
                                            <div class="item-inner">
                                                <span class="size-label">M</span>
                                            </div>
                                        </div>
                                        <!--item-->
                                        <div class="property-item">
                                            <input type="radio" name="quickview_sizes" id="L">
                                            <div class="item-inner">
                                                <span class="size-label">L</span>
                                            </div>
                                        </div>
                                        <!--item-->
                                        <div class="property-item">
                                            <input type="radio" name="quickview_sizes" id="XL">
                                            <div class="item-inner">
                                                <span class="size-label">XL</span>
                                            </div>
                                        </div>
                                    </div>
                                </div id="color-properties">
                            </div>
            
                            <!--Description-->
                            <div class="quickview-description content has-slimscroll">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.Scrupulum, inquam, abeunti; Ubi ut eam
                                    caperet aut quando? Erat enim
                                    Polemonis. Utram tandem linguam nescio? Duo Reges: constructio interrete. </p>
            
                                <p>Alio modo Non est igitur voluptas bonum. Estne, quaeso, inquam, sitienti in bibendo
                                    voluptas? Erat enim Polemonis. Minime vero,
                                    inquit ille, consentit. Hic ambiguo ludimur. Numquam facies. Ea possunt paria non esse. </p>
            
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Scrupulum, inquam, abeunti; Ubi ut eam
                                    caperet aut quando? Erat enim
                                    Polemonis. Utram tandem linguam nescio? Duo Reges: constructio interrete.</p>
                            </div>
            
                            <div class="quickview-controls">
                                <div class="spinner">
                                    <button class="remove">
                                        <i data-feather="minus"></i>
                                    </button>
                                    <span class="value">1</span>
                                    <button class="add">
                                        <i data-feather="plus"></i>
                                    </button>
                                    <input class="spinner-input" type="hidden" value="1">
                                </div>
                                <a class="button is-solid accent-button raised">
                                    <span>Add To Cart</span>
                                    <var id="quickview-button-price">27.00</var>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
    </div>
    
   
    <script type="text/javascript">
        function listProducts() {
            $.ajax({
                type: 'GET',
                url : "api.php",
                dataType : "html",
                cache : "false",
                data : {
                    action          : 'postDetails',
                    postId          : '<?php echo $postId;?>',
                },
                success : function(html, textStatus){
                    document.getElementById('pageContent').innerHTML = html;
                    showSlides(1);
                },
                error : function(xht, textStatus, errorThrown){
                    alert('Error: '+ errorThrown);
                }
            });
        }

        listProducts();
    </script>

    <script>
        var slideIndex = 1;

        function plusSlides(n) {
          showSlides(slideIndex += n);
        }

        function currentSlide(n) {
            showSlides(slideIndex = n);
        }

        function showSlides(n) {
            var i;
            var slides = document.getElementsByClassName("mySlides");
            var dots = document.getElementsByClassName("dot");
            if (n > slides.length) {slideIndex = 1}    
            if (n < 1) {slideIndex = slides.length}
            for (i = 0; i < slides.length; i++) {
              slides[i].style.display = "none";  
            }
            for (i = 0; i < dots.length; i++) {
              dots[i].className = dots[i].className.replace(" active", "");
            }
            slides[slideIndex-1].style.display = "block";  
            dots[slideIndex-1].className += " active";
        }
    </script>



    <!-- Concatenated js plugins and jQuery -->
    <script src="assets/js/app.js"></script>
    
    <!-- Core js -->
    <script src="assets/js/global.js"></script>
    <script src="assets/js/main.js"></script>
    
    <!-- Page and UI related js -->
    <script src="assets/js/feed.js"></script>
    <script src="assets/js/stories.js"></script>
    <script src="assets/js/chat.js"></script>
    <script src="assets/js/inbox.js"></script>
    <script src="assets/js/profile.js"></script>
    <script src="assets/js/friends.js"></script>
    <script src="assets/js/events.js"></script>
    <script src="assets/js/explorer.js"></script>
    <script src="assets/js/news.js"></script>
    <script src="assets/js/questions.js"></script>
    <script src="assets/js/videos.js"></script>
    <script src="assets/js/shop.js"></script>
    <script src="assets/js/settings.js"></script>
    
    <!-- Components js -->
    <script src="assets/js/widgets.js"></script>
    <script src="assets/js/autocompletes.js"></script>
    <script src="assets/js/modal-uploader.js"></script>
    <script src="assets/js/popovers-pages.js"></script>
    <script src="assets/js/go-live.js"></script>
    <script src="assets/js/lightbox.js"></script>
    <script src="assets/js/touch.js"></script>
    
</html>