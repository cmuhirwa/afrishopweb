<?php 
// START INITIATE
	if ($_SERVER["REQUEST_METHOD"] == "POST" || $_SERVER["REQUEST_METHOD"] == "GET") 
	{
		$apiUrl = 'http://165.22.82.105';
		$request = array_merge($_POST, $_GET);
		if(isset($request['action']))
		{
			header('Content-Type: application/json');
			echo $request['action']();
		}
		else
		{
			echo 'Please read the API documentation';
		}
	}
	else
	{
		echo 'AFRISHOP API V01';
	}
// END INITIATE

//  START ACCOUNTS
	function postDetails(){
		global $request;  
		global $apiUrl;  
		$postId     = $request['postId'];    
		$url 		= $apiUrl.'/community/'.$postId;
		// CALL MTN
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_PORT => "8080",
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 360,
			CURLOPT_SSL_VERIFYHOST => false,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => array(
			"Content-Type: application/x-www-form-urlencoded"
			),
		));

		$response = json_decode(curl_exec($curl));
		$err = curl_error($curl);
		$contentList = $response->{'contentList'};

		curl_close($curl);

		if ($err) {
			echo $err;
		} 
		else 
		{

			$curl = curl_init();

			curl_setopt_array($curl, array(
				CURLOPT_PORT => "8080",
				CURLOPT_URL => 'http://165.22.82.105:8080/commentsByPostId/'.$postId.'?pageNo=0&pageSize=10',
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 360,
				CURLOPT_SSL_VERIFYHOST => false,
				CURLOPT_SSL_VERIFYPEER => false,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "GET",
				CURLOPT_HTTPHEADER => array(
				"Content-Type: application/x-www-form-urlencoded"
				),
			));

			$commentsResponse = json_decode(curl_exec($curl));
			$err = curl_error($curl);
			//$contentList = $commentsResponse->{'contentList'};
			curl_close($curl);
			?>
			<div id="shop-page" class="shop-wrapper">
                <div class="store-sections">
                    <div class="container">
            
                        <!--Products-->
                        <div id="products-tab" class="store-tab-pane is-active">
                            <div class="columns is-multiline">
                                <a class="quickview-trigger"> <i data-feather="more-horizontal"></i> </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

				<a class="mobile-sidebar-trigger is-story-post is-home-v2">
                    <i data-feather="menu"></i>
                </a>
        

        		
                <!-- Story -->
                <div class="story-post-wrapper">
                    <div class="story-post">
                        <div class="post-title">
                            <h2><?php echo $response->{'title'};?></h2>
                            <!--Dropdown-->
                        </div>
                        <div class="post-image-wrap">
	                        <div class="slideshow-container">
	                        	<?php 
	                        		for ($i = 0; $i < count($contentList); $i++) 
	                        		{
	                        			$n= $i+1;
	                        			echo 
	                        				'<div class="mySlides fade" >
												<div class="numbertext">'.$n.'/'.count($contentList).'</div>
												<img src="'.$contentList[$i]->{'content'}.'" style="height: 400px; width: 100%; display: block; object-fit: cover; border-radius: 24px;">
											</div>';
	                        		} 
	                        	?>
			                    <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
			                    <a class="next" onclick="plusSlides(1)">&#10095;</a>

		                    </div>
		                    <br>

		                    <div style="text-align:center">
		                    	<?php for ($i = 0; $i < count($contentList); $i++) 
	                        		{
	                        			$n= $i+1;
	                        			echo 
	                        				'<span class="dot" onclick="currentSlide('.$n.')"></span>';
	                        		} 
	                        	?>
		                    </div>
		                </div>
                        <div class="post-meta">
                            <div class="post-author">
                                <div class="story-avatar">
                                    <img class="avatar" src="<?php echo $response->{'user'}->{'avatar'};?>"
                                        data-demo-src="assets/img/avatars/stella.jpg" data-user-popover="2" alt="">
                                </div>
                                <div class="meta">
                                    <span><?php echo $response->{'user'}->{'username'};?></span>
                                    <span>2 hours ago</span>
                                </div>
                            </div>
        
                            <div class="post-stats">
                                <div class="stat-item">
                                    <i data-feather="heart"></i>
                                    <span><?php echo $response->{'likes'};?></span>
                                </div>
                                <div class="stat-item">
                                    <i data-feather="message-circle"></i>
                                    <span><?php echo $response->{'comments'};?></span>
                                </div>
                            </div>
                        </div>
                        <div class="post-text content">
                            <p><?php echo $response->{'caption'};?></p>
        
                        </div>
                        <div class="post-tags">
                            <div class="tags">
                            	<?php 
	                            	for ($i = 0; $i < count($contentList); $i++) 
	                            	{ 
	                            		$tagList = $contentList[$i]->{'tagList'};
	                            		for ($k = 0; $k < count($tagList); $k++) 
	                            		{
	                            			echo '<span class="tag" data-user-popover="1">#'.$tagList[$k]->{'tag'}->{'tagName'}.'</span>';
	                            		}

	                            	} 
                        		?>
                            </div>
                        </div>
                        <div class="post-compose">
                            <div class="control">
                                <textarea class="textarea" placeholder="Post a comment..."></textarea>
                            </div>
                            <div class="compose-controls">
                                <img class="avatar" src="https://via.placeholder.com/150x150" data-demo-src="assets/img/avatars/jenna.png" data-user-popover="0" alt="">
                                <div class="compose-actions">
                                    <a class="action">
                                        <i data-feather="at-sign"></i>
                                    </a>
                                    <a class="action">
                                        <i data-feather="image"></i>
                                    </a>
                                    <a class="action">
                                        <i data-feather="paperclip"></i>
                                    </a>
                                </div>
                                <a class="button is-solid accent-button raised">Post Comment</a>
                            </div>
                        </div>
        
                        <div class="comments-wrap">
                            <div class="comments-count">
                                <h3>Comments (<?php echo $response->{'comments'};?>)</h3>
                            </div>
            				
            				<?php 
                                if(count($commentsResponse)>0)
                                {
                        			for ($m = 0; $m < count($commentsResponse); $m++) 
                            		{
                            			
                            			$comment 	= $commentsResponse[$m]->{'content'};
                            			$commentDate 	= $commentsResponse[$m]->{'date'};
                            			$commentaVatar	= $commentsResponse[$m]->{'user'}->{'avatar'};
                            			$commentUsername = $commentsResponse[$m]->{'user'}->{'username'};
                            	?>  
		                            <div class="media is-comment">
		                                <div class="media-left">
		                                    <div class="avatar-wrap is-smaller">
		                                        <img src="<?php echo $commentaVatar;?>" data-demo-src="assets/img/avatars/dan.jpg" data-user-popover="1" alt="">
		                                        <div class="badge">
		                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check"><polyline points="20 6 9 17 4 12"></polyline></svg>
		                                        </div>
		                                    </div>
		                                </div>
		                                <div class="media-content">
		                                    <div class="comment-meta">
		                                        <h4><a><?php echo $commentUsername;?></a> <small> · 3 hours ago</small></h4>
		                                        <p><?php echo $comment;?></p>
		                                    </div>
		                                    <div class="comment-stats-wrap">
		                                        <div class="comment-stats">
		                                            <div class="stat is-likes">
		                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-thumbs-up"><path d="M14 9V5a3 3 0 0 0-3-3l-4 9v11h11.28a2 2 0 0 0 2-1.7l1.38-9a2 2 0 0 0-2-2.3zM7 22H4a2 2 0 0 1-2-2v-7a2 2 0 0 1 2-2h3"></path></svg>
		                                                <span>23</span>
		                                            </div>
		                                            <div class="stat is-dislikes">
		                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-thumbs-down"><path d="M10 15v4a3 3 0 0 0 3 3l4-9V2H5.72a2 2 0 0 0-2 1.7l-1.38 9a2 2 0 0 0 2 2.3zm7-13h2.67A2.31 2.31 0 0 1 22 4v7a2.31 2.31 0 0 1-2.33 2H17"></path></svg>
		                                                <span>3</span>
		                                            </div>
		                                        </div>
		                                        <div class="comment-actions">
		                                            <a class="comment-action is-like">
		                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-thumbs-up"><path d="M14 9V5a3 3 0 0 0-3-3l-4 9v11h11.28a2 2 0 0 0 2-1.7l1.38-9a2 2 0 0 0-2-2.3zM7 22H4a2 2 0 0 1-2-2v-7a2 2 0 0 1 2-2h3"></path></svg>
		                                            </a>
		                                            <a class="comment-action is-dislike">
		                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-thumbs-down"><path d="M10 15v4a3 3 0 0 0 3 3l4-9V2H5.72a2 2 0 0 0-2 1.7l-1.38 9a2 2 0 0 0 2 2.3zm7-13h2.67A2.31 2.31 0 0 1 22 4v7a2.31 2.31 0 0 1-2.33 2H17"></path></svg>
		                                            </a>
		                                            <a class="comment-action is-reply">
		                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-message-circle"><path d="M21 11.5a8.38 8.38 0 0 1-.9 3.8 8.5 8.5 0 0 1-7.6 4.7 8.38 8.38 0 0 1-3.8-.9L3 21l1.9-5.7a8.38 8.38 0 0 1-.9-3.8 8.5 8.5 0 0 1 4.7-7.6 8.38 8.38 0 0 1 3.8-.9h.5a8.48 8.48 0 0 1 8 8v.5z"></path></svg>
		                                            </a>
		                                        </div>
		                                    </div>
		                                </div>
		                            </div>
		                    <?php 
		                    		}
            					}
                            ?>
                        </div>
                    </div>
                </div>
        
                <!--Story post sidebar-->
                <div class="story-post-sidebar">
                    <div class="header">
                        <h2>Items</h2>
                    </div>
                    <div class="related-posts">
                        <!--Related post-->
                        <?php 
                        	for ($i = 0; $i < count($contentList); $i++) 
                        	{ 
                        		$tagList = $contentList[$i]->{'tagList'};
                        		for ($k = 0; $k < count($tagList); $k++) 
                        		{
                        			echo 
                        			'<a class="related-post">

			                            <img class="post-image" src="'.$tagList[$k]->{'tag'}->{'item'}->{'itemImg'}.'"
			                                data-demo-src="'.$tagList[$k]->{'tag'}->{'item'}->{'itemImg'}.'" alt="">
			                            <div class="meta">
			                                <h3>'.$tagList[$k]->{'tag'}->{'item'}->{'itemName'}.'</h3>
			                                <div class="user-line">
			                                    <span>$'.$tagList[$k]->{'tag'}->{'item'}->{'discountPrice'}.'</span>
			                                </div>
			                            </div>
			                        </a>';
                        		}

                        	} 
                		?>
                        
                    </div>
                </div>

			<?php
		}

	}
